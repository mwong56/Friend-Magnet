package com.mwong56.friendmagnet.activities;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.Menu;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mwong56.friendmagnet.R;
import com.parse.ParseUser;
import com.parse.PushService;

public class MainActivity extends FragmentActivity {
	private ParseUser user_;
	private GoogleMap gMap_;
	private BroadcastReceiver receiver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		user_ = ParseUser.getCurrentUser();
		PushService.setDefaultPushCallback(getApplicationContext(),
				MainActivity.class);
		PushService.subscribe(getApplicationContext(),
				"user_" + user_.getObjectId(), MainActivity.class);

		IntentFilter iFilter = new IntentFilter();
		iFilter.addAction("com.mwong56.friendmagnet.RECEIVE_GPS");
		receiver = new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {
				try {
					JSONObject json = new JSONObject(intent.getExtras()
							.getString("com.parse.Data"));
					String latitude = (String) json.get("latitude");
					String longitude = (String) json.get("longitude");
					String user = (String) json.get("user");
					gMap_.addMarker(new MarkerOptions().position(
							new LatLng(Double.parseDouble(latitude), Double
									.parseDouble(longitude))).visible(true));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		registerReceiver(receiver, iFilter);
	}

	@Override
	protected void onStart() {
		super.onStart();
		FragmentManager fManager = getSupportFragmentManager();
		SupportMapFragment sMapFrag = (SupportMapFragment) fManager
				.findFragmentById(R.id.map);
		gMap_ = sMapFrag.getMap();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
