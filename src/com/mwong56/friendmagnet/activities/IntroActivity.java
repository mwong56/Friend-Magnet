package com.mwong56.friendmagnet.activities;

import com.mwong56.friendmagnet.R;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class IntroActivity extends Activity {

	private Button mRegisterButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Parse.initialize(this, "DN8VDaOKBRlfOUMub9XhyIEOj3raf7niEaOfaVG4",
				"RonA87I0IzxPIutqr2APdl700ay3phjERo1wCyLH");
		ParseFacebookUtils.initialize("656660614397647");
		ParseAnalytics.trackAppOpened(getIntent());
		checkForExistingLogin();
		setContentView(R.layout.activity_intro);
		initLayout();
		initListeners();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);
	}

	private void initLayout() {
		mRegisterButton = (Button) findViewById(R.id.registerButton);
	}

	private void checkForExistingLogin() {
		ParseUser currentUser = ParseUser.getCurrentUser();
		if (currentUser != null) {
			startMainActivity();
		}
	}

	private void startMainActivity() {
		Intent i = new Intent(IntroActivity.this, MainActivity.class);
		startActivity(i);
		finish();
	}

	private void initListeners() {
		mRegisterButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ParseFacebookUtils.logIn(IntroActivity.this,
						new LogInCallback() {
							@Override
							public void done(ParseUser user, ParseException err) {
								if (user == null) {
									Log.d("Facebook Login",
											"Uh oh. The user cancelled the Facebook login.");
								} else {
									startMainActivity();
								}
							}
						});
			}
		});
	}
}
